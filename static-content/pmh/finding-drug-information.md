![Image of couple at laptop](/core/assets/pmh/images/PMH_laptopcouple.jpg)

Finding information about a drug
================================

Do you want to start with basic information about how a drug is used? Or do you want to know what the research shows? Our site can help you. And we have tips about using medicines [here](/pubmedhealth/using-medicines/).

On this page we also show you where you can find more information about a drug—and how to find clinical trials to join.

1.  Basic information
    -----------------

    You can search on our site using the generic or brand name for a drug, or for a drug class. When there is a box labeled “About” a drug, clicking on a link there leads you to a page that explains the drug and its uses. 

    For drug classes, there could be a box too: that leads you to an explanation of the class. There might also be links to information on drugs included in that class. You can read more about drug names and classes [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/drug-names-and-classes/).

2.  Research results
    ----------------

    When you search on our site you will see results of reliable research on drugs. These report on in-depth reviews of research testing the effects of the drug on patients. Many of these reviews include summaries for consumers.
     Information based on this research is included in the sections marked “What works?” throughout this site. You can read more about this kind of research [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/).

3.  More technical information about a drug
    ---------------------------------------

    If you want to know more technical information about the chemical aspects of a drug, how it works and what the FDA recommends about it, you can find that information at another website from the National Library of Medicine called [DailyMed](http://dailymed.nlm.nih.gov/).

If you want to find out about clinical trials that may be recruiting participants, go to [ClinicalTrials.gov](http://clinicaltrials.gov/).

If your questions are about how to get access to drugs, contact your doctor or the drug’s manufacturer.

*By PubMed Health, 20 August 2015.*

[Approved drug uses](/pubmedhealth/approved-drug-uses/)

[Drug names and classes](/pubmedhealth/drug-names-and-classes/)

[Tips about using medicines](/pubmedhealth/using-medicines/)


