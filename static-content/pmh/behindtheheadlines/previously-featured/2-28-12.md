### Featured UK headline analysis

#### [Sleeping pills linked to death risk](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2012-02-28-sleeping-pills-linked-to-death-risk/)

28 February 2012

![Image for UK NHS headline](/core/assets/pmh/images/PMH_yawn.png)Sleeping pills are linked to a raised risk of death, according to several high-profile news reports in today’s newspapers. Several front page stories have covered the link, reporting a four-fold death risk among users of the drugs, medically known as ‘hypnotics’. The news is based on the results of a large US study that compared the medical records of more than 10,000 people prescribed sleeping pills and 23,000 similar people who had never been prescribed them. It followed them for an average of 2.5 years and found that people prescribed hypnotics, even at very low doses, were more likely to die than those not prescribed hypnotics.

### In the US

#### [Study Suggests a Link Between Sleeping Pills, Early Death](http://blogs.wsj.com/health/2012/02/28/study-suggests-a-link-between-sleeping-pills-early-death/)

The Wall Street Journal

A new study suggests that people who take sleeping pills are more likely to die within a couple of years than those who don’t, though it doesn’t prove

### Related Info

#### [Non-drug options](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004995/)

PubMed Health

Read about sleep and non-drug approaches to getting more of it here


