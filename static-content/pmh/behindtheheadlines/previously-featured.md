Previously Featured
===================

-   [UK : Memory loss 'reversed in monkeys'](/monkeys/)
     [US: Monkewy title for US](/aomwriunf)
     [Related Info: CER Review title](/ahah)
     <span style="font-size: 9px;">P</span><span style="font-size: 9px;">ublished: September 21, 2011</span>
-   [Bad sleep studies for blodd pressure risk](/pubmedhealth/behindtheheadlines/news/2011-09-01-bad-sleep-studied-for-blood-pressure-risk/)
     <span style="font-size: 9px;">P</span><span style="font-size: 9px;">ublished: August 22, 2011</span>
-   [Scientists put human brain in sheep](/pubmedhealth/human-brain-swap)
     <span style="font-size: 9px;">Published: July 21, 2011</span>


