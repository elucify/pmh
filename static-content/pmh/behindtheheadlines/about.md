[Read the latest headlines here](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/)

What is NHS' Behind the Headlines?
==================================

*From "Behind the Headlines", provided by NHS Choices (from England's National Health Service).*

Behind the Headlines provides an unbiased and evidence-based analysis of health stories that make the news.

The service is intended for both the public and health professionals, and endeavours to:

-   explain the facts behind the headlines and give a better understanding of the science that makes the news,
-   provide an authoritative resource for doctors which they can rely on when talking to patients, and
-   become a trusted resource for journalists and others involved in the dissemination of health news.

Behind the Headlines is the brainchild of Sir Muir Gray, chief knowledge officer of the NHS and a lifelong advocate of patient empowerment.

“Scientists hate disease and want to see it conquered," said Sir Muir. "But this can lead to them taking an overly optimistic view of their discoveries which is often reflected in newspaper headlines.

“Our service has more time to examine the science behind the stories. Independent experts check the findings and assess the research methods to provide a more considered view."

Sir Muir Gray has worked in public health for 35 years. He helped pioneer Britain's breast and cervical cancer screening programmes and was knighted in 2005 for the development of the foetal, maternal and child screening programme and the creation of the National Library for Health.

He is currently director of the National Knowledge Service and responsible for the National Library for Health.

### How the system works

-   Each day the NHS Choices team selects health stories that are making headlines.
-   These, along with the scientific articles behind the stories, are sent to [Bazian](http://www.bazian.com/), a leading provider of evidence-based healthcare information.
-   Bazian's clinicians and scientists analyse the research and produce impartial evidence-based assessments, which are edited and published by NHS Choices.

Read more about the [production process](http://www.nhs.uk/aboutNHSChoices/aboutnhschoices/Aboutus/Pages/produce-behind-headlines.aspx).

### Services

Behind the Headlines aims to respond to news stories the day they appear in the media.

Behind the Headlines is also available in [RSS format](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=behindtheheadlines).


