What works?
-----------

[Clinical effectiveness research](/~/aboutcer/) finds answers to the question “What works?” in medical and health care.

The effects of treatments and prevention methods are tested in clinical trials. But one trial is rarely enough to provide definite answers. So researchers search for, and then analyze, all the trials that have studied particular questions.

These clinical effectiveness reviews[](#_ftn1) can show what treatments and prevention methods have been proven to work—and what remains unknown.
  
  

[How do we know what works](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)

[How to read health news](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/how-to-read/)


