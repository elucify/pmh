![Woman with digital tablet](/core/assets/pmh/images/Blog_PubMed_sys-review-portlet.jpg)

PubMed Health’s curated collection of systematic reviews now has an important new role: enabling PubMed users to go straight from a clinical trial to systematic reviews that have considered it.

Visitors to records of many thousands of trials will now see a new section to the right, called a portlet. It will show links to systematic reviews in PubMed that have cited that trial.

For example, people visiting the NIH-funded [Women’s Health Study publication](http://www.ncbi.nlm.nih.gov/pubmed/10601381) on beta-carotene supplementation would see the “Cited by 9 systematic reviews” portlet pictured here.

The 9 systematic reviews are identified by PubMed as systematic reviews because they are included in PubMed Health. The most recent one comes first. In this case, it’s the 2013 [systematic review of supplements to prevent disease](http://www.ncbi.nlm.nih.gov/pubmed/24308073) done by AHRQ, the U.S. Agency for Healthcare Research and Quality, for the U.S. Preventive Services Task Force.

![Screen shot of systematic review portlet](/core/assets/pmh/images/Screenshot-sys-review-portlet.jpg)

This new portlet does not replace “Related citations in PubMed”: that will follow as usual. All of PubMed Health’s 31,100 systematic reviews since 2003 are not yet included.

If there is no portlet showing, it does not mean for certain that there is no systematic review that includes the trial. There are enough reviews included, though, that this portlet should become a familiar sight when you’re using PubMed.

One trial is rarely enough to provide [definitive answers](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/). The results of one trial can seem to be contradicted by another. Knowing it’s part of a systematic review is an important step in helping people look at evidence in context. We hope it introduces many more people to the great work done by so many thousands of systematic reviewers around the world.

So remember, when you look at a trial in PubMed – glance to the right to see how this trial fits into the bigger picture of effectiveness research. We hope you find it useful.

 *The PubMed Health team*

**More about:**

[Finding systematic reviews at PubMed Health and in PubMed](http://www.ncbi.nlm.nih.gov/pubmedhealth/finding-systematic-reviews/)

[Systematic reviews and clinical effectiveness research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)


