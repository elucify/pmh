![Children eating a healthy lunch](/core/assets/pmh/images/Blog_children_heart_health_032615.jpg)

The rate of childhood obesity has been rising – and their risk of heart disease later in life along with it. The problem might be worse than we realize. [A new study](http://www.ncbi.nlm.nih.gov/pubmed/25782775) <span>concluded that between 2003 and 2010, over 80% of children in the U.S. had a poor diet.</span>

According to the team from [NHS Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2015-03-18-damage-to-heart-health-may-start-in-childhood/), the large survey behind these figures should be nationally representative. Called NHANES, the [National Health and Nutrition Examination Survey](http://www.cdc.gov/nchs/nhanes/about_nhanes.htm) is run by the Centers for Disease Control and Prevention (CDC).

The Behind the Headlines point out that the results can’t be exact. They point to reasons such as the survey being based on people’s memories of what was consumed – and the ideal dietary intakes weren’t based on the individual children’s age and other key characteristics. You can read more about it in PubMed Health’s Behind the Headlines feature.

How could this trend be turned around? Check out these [systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) at PubMed Health:

-   Getting under 5s to eat more [fruits and vegetables](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0049730/) (fruit was easier!)
-   [Obesity prevention programs](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011235/) to support <span>parents and schools to encourage healthier diets and physical activity in children</span>
-   [School programs for physical activity](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014555/) and fitness in children and teenagers

And find out more about [preventing hypertension](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0024200/) for adults, too.

*The PubMed Health Team*

[Featured at Behind the Headlines on PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2015-03-18-damage-to-heart-health-may-start-in-childhood/)

[The NHANES study in PubMed](http://www.ncbi.nlm.nih.gov/pubmed/25782775)


