![Image of man at laptop.](/core/assets/pmh/images/PMH_filters.jpg)

There’s a lot of information at [PubMed Health](/pubmedhealth/%20) now —and lots more is added every week. So we’ve added search filters to help you narrow down the results.

A search filter is a shortcut that tells PubMed Health to "show me only articles like this". For example, if you want to see only our evidence-based consumer information on cancer, now you can type this into the search box:

                cancer AND pmh\_cons[sb]

Only that type of article will show on the results page—[click here to see this](/pubmedhealth/?term=cancer+AND+pmh_cons%5Bsb%5D%20).

Make sure you type AND in capital letters: that’s part of how PubMed databases recognize a special combined request. It’s called a Boolean operator. Others are OR and NOT. You can find out more about [Boolean operators and how to combine them here](http://www.nlm.nih.gov/bsd/disted/pubmedtutorial/020_350.html%20).

You can read all about the filters in the new page on [finding systematic reviews](/pubmedhealth/finding-systematic-reviews/%20%20) at PubMed Health. There are filters for [systematic reviews](/pubmedhealth/aboutcer/%20) and subsets of them (including those from DARE—the Database of Abstracts of Reviews of Effects—or from Cochrane), as well as evidence-based consumer information and information for clinicians.

And you can find out the most recent entries to PubMed Health by adding \_month:

                cancer AND pmh\_cons\_month[sb]

Now [only articles added in the last month](/pubmedhealth/?term=cancer+AND+pmh_cons_month%5Bsb%5D%20) will show.

There’s an even quicker way to see recently added systematic reviews and evidence-based information at our new ["What’s New" page](/pubmedhealth/new/%20). This appears in the navigation bar on every page.

Watch out for more developments to help you find what you need at PubMed Health here at [the blog](/pubmedhealth/blog/), [Twitter](https://twitter.com/PubMedHealth), [Facebook](https://www.facebook.com/PubMedHealth), or [Google+](https://plus.google.com/107599362190099097644/posts). You can sign up to subscribe to the blog by email, too.

*The PubMed Health Team*

**Related pages on PubMed Health:**

-   [What’s New ](/pubmedhealth/new/%20)
-   [Finding systematic reviews](/pubmedhealth/finding-systematic-reviews/%20)


