![Teenage boy wearing baseball cap](/core/assets/pmh/images/Sunburn-melanoma-risk-photo.jpg)

Can just five serious sunburns when you are a teenager increase your risk of developing skin cancer? It can, according to the authors of a study analyzed by the team from [NHS Behind the Headlines](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-06-04-just-five-sunburns-increase-your-cancer-risk/) this week. The report comes from the [Nurses' Health Study](http://cebp.aacrjournals.org/content/early/2014/05/16/1055-9965.EPI-13-0821.abstract?sid=0dc561ae-bba4-4557-a2b9-bd77c3001c6d), which included the experiences of nearly 110,000 American women.

It can be very hard to be sure about sun exposure and [UV rays](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0022667/)—and even harder to get it right years later. Do you remember how many times you got sunburnt in a particular 5-year period? Find out how the researchers addressed this problem in the [Behind the Headlines story](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/news/2014-06-04-just-five-sunburns-increase-your-cancer-risk/). 

Even if you’re not a teenager, it’s definitely not too late. Check out the [evidence-based information on skin cancer prevention](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0032743/) from the National Cancer Institute at PubMed Health.

 *The PubMed Health Team*


