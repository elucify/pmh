![Photo of woman sitting in woods](/core/assets/pmh/images/146025481_225.jpg)

Itching, sneezing, and runny noses. At this time of year, getting longterm relief from allergies is on even more people’s minds than usual.

According to [a new systematic review](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0055886/) from the US Agency for Healthcare Research and Quality (AHRQ), allergy shots can help adults and children with single, specific allergies. Relieving the allergy could reduce asthma related to those allergies as well.

The formal name for this treatment is specific immunotherapy or SIT. It’s also called desensitization or hypo-sensitization. It involves a series of injections with increasing amounts of the allergen –  substances such as pollens - that triggers their particular allergy.

The goal of SIT is to de-sensitize a person to that allergen. The person is watched carefully to make sure that they don’t suffer an asthma attack or severe reaction.

AHRQ’s researchers found 74 trials of these allergy shots. They concluded there’s enough evidence on both safety and effectiveness of single-allergy SIT, for adults and children. More research is needed on SIT that acts on multiple allergens at once – especially in children.

The AHRQ researchers also looked at the evidence on using drops or tablets under the tongue. This is called oral or sub-lingual immunotherapy (OIT and SLIT). They explain that [this has not been approved for use by the FDA](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0055874/). So in the US, this form of the treatment is called [off-label use](http://www.ncbi.nlm.nih.gov/pubmedhealth/approved-drug-uses/%20).

For more details, see the [researchers’ summary](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0055879/) of their review or go to the [full text](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0055886/).

There is more information on specific immunotherapy – including consumer information - [here at PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=specific+immunotherapy%20). Keep in mind that research and information comes from researchers internationally, so it won’t always explain the situation in the US.

*The PubMed Health Team*

**Keeping up with systematic reviews added to PubMed Health:**

[Click here](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/05/new-filters-shortcuts/%20) to find out how to narrow these down to subjects that interest you.

***New and updated Cochrane reviews:***

[77 new or updated reviews on healthcare interventions in Issue 4 of 2013](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=pmh_cc_month%5Bsb%5D%20)

***Reviews added to DARE (Database of Abstracts of Reviews of Effects):***

[Reviews on healthcare interventions in the last month](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=pmh_dare_month%5Bsb%5D)– 781 as of today

***Other systematic reviews:***

[Full text reviews from public agencies in the last month](http://www.ncbi.nlm.nih.gov/pubmedhealth/s/subject_full_text_reviews_medrev/?term=pmh_sr_other_month%5bsb) – 13 as of today


