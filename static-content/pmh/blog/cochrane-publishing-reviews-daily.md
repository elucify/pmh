![Image of man with laptop.](/core/assets/pmh/images/PMH_manlaptop_blog.jpg)

Cochrane reviews are now being published as soon as they are ready, rather than waiting for once-a-month release. PubMed Health will release the new and updated reviews on a continuous basis too.

This means that there could be new or updated Cochrane reviews on PubMed Health every weekday. It usually takes a day for us to process and release new Cochrane reviews.

[Cochrane had moved](http://ims.cochrane.org/support/crgs/publication-frequency) from quarterly to monthly releases in 2010. The recently released May issue of the Cochrane Collaboration's systematic review journal, the Cochrane Database of Systematic Reviews (CDSR), was the last to come in a single batch. 

Individual reviews will still be part of [a single month’s issue](http://www.thecochranelibrary.com/view/0/13996979657a.html)—it's just that every review released within that month will comprise the issue. That means the June issue is streaming through now. 

If you want to see which Cochrane reviews have been published in the last week at [PubMed Health](http://www.ncbi.nlm.nih.gov/pubmedhealth/), enter this into our search box: pmh\_cc\_week[sb]  

We’ll be letting you know how many reviews have been added to PubMed Health on a weekly basis through the [PubMed Health Blog](/pubmedhealth/blog/). 

*The PubMed Health Team*

Cochrane reviews of interventions published [in the last week](/pubmedhealth/?term=pmh_cc_week%5Bsb%5D)  

Cochrane reviews of interventions published [in the last month](/pubmedhealth/?term=pmh_cc_month%5Bsb%5D) (including 70 from the May issue) 

[All 208 systematic reviews](/pubmedhealth/?term=pmh_sr_week%5Bsb%5D) added to PubMed Health in the last week (as at blog time) 

More about [PubMed Health filters](/pubmedhealth/finding-systematic-reviews/). 


