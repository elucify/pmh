![Research sign on desk](/core/assets/pmh/images/Blog-methods-resources-060215.jpg)

What’s research on research methods, you might ask, and what’s it doing at PubMed Health?

We specialize in [clinical effectiveness research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/) <span>at PubMed Health – that’s research designed to give answers that are as reliable as possible to the question, “What works?”</span>

<span>Getting to more reliable answers is a complex research task. You need to do it as well as possible every step of the way – from working out how to go about getting your question right to communicating your results when you’re done. Methods research helps researchers do that: it looks for the answer to the question, “What works in research?”</span>

We have introduced a new category called “Methods resources.” You can browse the documents alphabetically by choosing that category in the [drop-down "Contents" menu](http://www.ncbi.nlm.nih.gov/pubmedhealth/s/methods_resources_medrev/a/) on the blue navigation bar.

![PubMed Health methods filter](/core/assets/pmh/images/Methods-filter-blog-060215.jpg)

Or you can use filters to narrow down your search results using the options on the left of the screen. For tips on this, check out our blog post about [refining your search](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2013/07/refine-search-results-PubMedHealth-faceted-search/). 

The methods resources are broken down into 3 sub-categories: methods guidance, methods studies, and methods systematic reviews. You can narrow it down to one of these by clicking on the link lower down.

You can also narrow the search down to one provider, [like AHRQ](http://www.ncbi.nlm.nih.gov/pubmedhealth/?term=methods_resources[sb]&filters=ahrq).

What’s in those 3 sub-categories?

*Methods guidance*<span> includes documents that provide advice on methods to use for a particular purpose. Examples from AHRQ include:</span>

<span>► </span>[<span>How to do systematic reviews of medical tests</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0077863/)

<span>► </span>[<span>How to avoid bias in selecting studies for systematic reviews</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0077773/%20)

*Methods studies* are a mixture. They include studies comparing or simulating techniques, evaluations, and studies developing or validating (verifying) new techniques. More examples from AHRQ:

<span>► </span>[<span>A pilot study of machine learning to help update systematic reviews</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0077728/)

<span>► </span><span>[Developing a framework to use the results of systematic reviews to design new trials](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0076923/)</span>

*Methods systematic reviews* are systematic reviews of methods studies. These include:

<span>► </span><span>[IQWiG's review of surrogate outcomes (like biomarkers) in cancer studies](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0065194/%20)</span>

<span>► </span>[<span>AHRQ's review of communication and d</span><span>issemination strategies to get evidence use</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0063323/)<span></span>

<span>► </span>[<span>A Co</span><span>chrane review of studies comparing the results of trials reported in trial protocols and registries with published articles</span>](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0077934/)

The methods resources collection is a small category at the moment, with just over 140 documents, mostly from AHRQ. But it will be growing – and joined by some other exciting developments we’re working on with our partners. So stay tuned!

*The PubMed Health Team*

More about:

[An update of developments in the last year at PubMed Health](https://webmeeting.nih.gov/p154a763mba/?launcher=false&fcsContent=true&pbMode=normal%20) (22 minutes)

[Finding systematic reviews at PubMed Health and in PubMed](http://www.ncbi.nlm.nih.gov/pubmedhealth/finding-systematic-reviews/)

[Systematic reviews and clinical effectiveness research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/%20)


