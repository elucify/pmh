Disclaimer
----------

### Medical Information:

It is not  the intention of NCBI or NLM to provide specific medical advice but rather to provide users with information to better understand their health and their diagnosed disorders. Specific medical advice will not be provided, and NCBI and NLM urges you to consult with a qualified physician for diagnosis and for answers to your personal questions.

### Liability:

For documents and software available from this server, the U.S. Government does not warrant or assume any legal liability or responsibility for the accuracy, completeness, or usefulness of any information, apparatus, product, or process disclosed.

### Endorsement:

NCBI does not endorse or recommend any commercial products, processes, or services. The views and opinions of authors expressed on NCBI's Web sites do not necessarily state or reflect those of the U.S. Government, and they may not be used for advertising or product endorsement purposes.

### External Links:

Some NCBI Web pages may provide links to other Internet sites for the convenience of users. NCBI is not responsible for the availability or content of these external sites, nor does NCBI endorse, warrant, or guarantee the products, services, or information described or offered at these other Internet sites. Users cannot assume that the external sites will abide by the same Privacy Policy to which NCBI adheres. It is the responsibility of the user to examine the copyright and licensing restrictions of linked pages and to secure all necessary permissions.

### Pop-Up Advertisements:

When visiting our Web site, your Web browser may produce pop-up advertisements. These advertisements were most likely produced by other Web sites you visited or by third party software installed on your computer. The NLM does not endorse or recommend products or services for which you may view a pop-up advertisement on your computer screen while visiting our site.

### Conditions of Use:

This site is maintained by the U.S. Government and is protected by various provisions of Title 18 of the U.S. Code. Violations of Title 18 are subject to criminal prosecution in a federal court. For site security purposes, as well as to ensure that this service remains available to all users, we use software programs to monitor traffic and to identify unauthorized attempts to upload or change information or otherwise cause damage. In the event of authorized law enforcement investigations and pursuant to any required legal process, information from these sources may be used to help identify an individual.


