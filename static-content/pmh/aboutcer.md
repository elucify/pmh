![Clinically Effective stamp](/core/assets/pmh/images/PMH_clinicallyeffective.jpg)

About Clinical Effectiveness Research
=====================================

Clinical effectiveness research finds answers to the question “What works?” in medical and health care.

"Working" is a real health benefit - like symptom relief, quicker recovery, or longer life. To find out if something really works, all important effects need to be studied. That means possible harms as well as possible benefits.

Clinical or health effects are sometimes called [patient-relevant outcomes](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005160/)<sup>[](#_ftn1)</sup>.

**How do researchers get from an idea to proof of clinical effectiveness?**
---------------------------------------------------------------------------

Ideas about what could work might come from laboratory tests. There might be animal testing. Studies observing patients also generate important knowledge and theories.

But all these types of research cannot provide definite proof that a particular treatment works. Many other factors could be having an impact at the same time as treatment. People often improve with or without treatment, too.

**Putting ideas, theories, and beliefs to the test**
----------------------------------------------------

Testing clinical effectiveness in people requires experiments that can single out the true effects of specific actions. That is why the possible effects of treatments and prevention methods need to be studied in [clinical trials](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005079/)<sup>[](#_ftn2)</sup>[](#_ftn2).

One trial is rarely enough to provide definite answers. Later trials sometimes confirm early results - and sometimes come up with conflicting results. So researchers search for, and then analyze, all the trials that have studied particular questions. This type of research is called a systematic review. It can be used to look for studies other than trials as well, and address other questions - like, "What causes this disease?"

**Definite answers and areas of uncertainty**
---------------------------------------------

[Systematic reviews](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005078/)<sup>[](#_ftn3)</sup> can show which treatments and prevention methods have been proven to work - and what remains unknown. Being clear about what is certain or uncertain is an important part of [informed decision-making](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005210/)<sup>[](#_ftn4)</sup>.

Systematic reviews are the basis for what is often called evidence-based medicine or health care. And they are important for pointing to areas where more research is needed.

Other names for this kind of research are "evidence syntheses", "comparative effectiveness reviews", and "health technology assessments".

*By PubMed Health, reviewed 27 October 2015*

[How to read health news](http://www.ncbi.nlm.nih.gov/pubmedhealth/behindtheheadlines/how-to-read/)

[What to know about screening](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033150/)

[Why randomization is important](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005079/)


