**Drugs in a drug class have similarities, but they are not exactly the same.**

A prescribed drug can have two names: a **substance name** and a **brand name**. [Read more about drug classes and drug names](/pubmedhealth/drug-names-and-classes/)


