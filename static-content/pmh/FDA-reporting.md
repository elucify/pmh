![description of image](/core/assets/pmh/images/PMH_manphone.jpg)

Reporting side effects to the FDA
=================================

If you experience unexpected side effects after using a drug, there are a few ways you may report the problem to the Food and Drug Administration (FDA):

-   **Online:** Fill out the [interactive reporting form](https://www.accessdata.fda.gov/scripts/medwatch/medwatch-online.htm)
-   **Mail:** Download a copy of the pre-addressed, postage-paid [FDA 3500 Form](http://www.fda.gov/Safety/MedWatch/HowToReport/DownloadForms/default.htm) or call 1-800-FDA-1088 to ask for a copy.
-   **Fax:** Get a copy of the form described above and fax it to 1-800-FDA-0178.
-   **Phone:** Call FDA at 1-800-FDA-1088, Monday through Friday from 8 am to 4:30 pm EST.

Tips from the FDA
-----------------

Report what happened as soon as possible after you discover a problem.

Keep the product packaging and label. These include codes, numbers, and dates that will help FDA trace the product.

The FDA recommends reporting the problem to the drug’s manufacturer and the store where the product was bought as well as to them.

See the [FDA website](http://www.fda.gov/ForConsumers/ConsumerUpdates/ucm095859.htm) for more information. If you have questions, you can also call your local [FDA Consumer Complaint Coordinator](http://www.fda.gov/Safety/ReportaProblem/ConsumerComplaintCoordinators/default.htm).

*By PubMed Health, 18 September 2012.*

[More information about a drug](/pubmedhealth/finding-drug-information/)

[Tips about using medicines](/pubmedhealth/using-medicines/)


