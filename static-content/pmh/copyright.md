Copyright and Restrictions
--------------------------

PubMed Health contains both copyrighted and non-copyrighted material. Please check the copyright statement for specific types of content in PubMed Health before use. If content is copyrighted, transmission or reproduction of protected items beyond that allowed by fair use as defined in the copyright laws may require the written permission of the copyright owners. PubMed Health cannot grant this permission.

Works produced by employees of the U.S. Government as part of their official duties are not copyrighted within the U.S. If content is not copyrighted, you may reproduce and redistribute the content. However, it is requested that in any subsequent use of this work, the authors, publishers, NCBI and NLM be given appropriate acknowledgment.

Works produced by organizations other than the federal government are generally protected by U.S. and international copyright laws. Because in the U.S. and many foreign countries, the use of a copyright notice or symbol is optional in the display of copyrighted material, you are responsible for determining the copyright status of material for any use beyond that allowed by fair use in U.S. Copyright Law 17 U.S.C. 107.

Content on PubMed Health that is copyrighted includes, but is not limited to, images from [ThinkStockPhotos](http://www.thinkstockphotos.com/Legal/LicenseInformation).

The Cochrane Collaboration logo is a registered trademark of the Cochrane Collaboration.

Restrictions on Systematic Downloading of Content
-------------------------------------------------

Crawlers and other automated processes may NOT be used to systematically retrieve content from the PubMed Health web site and bulk downloading of content is prohibited.


