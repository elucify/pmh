Featured review
---------------

![Image of man for featured review.](/core/assets/pmh/images/PMH_pensive_guy.png)
Frozen shoulder is the common term for adhesive capsulitis. It’s a sore, stiff shoulder that isn’t caused by injury.

A frozen shoulder can take 1 to 3 years to get better by itself.

England’s National Institute for Health Research (NIHR) wanted to know about the best treatments for this common and painful condition. They commissioned a research team led by the University of York to look for evidence about what works.

The researchers concluded that in people who have had frozen shoulder for more than six months, these things can help:

-   Having 1 steroid injection, along with a home exercise program
-   Adding 4 weeks of physiotherapy that includes mobilization of the shoulder (8 to 10 physio sessions)

You can read more about the research results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0041076/).

You can see their entire research report here:

Maund E, Craig D, Suekarran S, Neilson AR, Wright K, Brealey S, Dennis L, Goodchild L, Hanchard N, Rangan A, Richardson G, Robertson J, McDaid C. Management of frozen shoulder: a systematic review and cost-effectiveness analysis. Health Technol Assess [Internet]. 2012 [cited 2012 Apr 25];16(11). [Full text](http://www.hta.ac.uk/fullmono/mon1611.pdf%20%20)


