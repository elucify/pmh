Featured review
---------------

![Image for ear tubes featured review.](/core/assets/pmh/images/PMH_eartube.jpg)
When might ear ventilation tubes be worthwhile for chronic middle ear infections called otitis media with effusion? Read about the background and results of a Cochrane review [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0016275/).

Or go straight to the Cochrane summary of results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0011194/).

Citation: Browning GG, Rovers MM, Williamson I, Lous J, Burton MJ. Grommets (ventilation tubes) for hearing loss associated with otitis media with effusion in children. Cochrane Database of Systematic Reviews 2010, Issue 10. CD001801.


