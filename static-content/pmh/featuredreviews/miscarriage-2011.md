Featured review
---------------

![Image of woman for featured review.](/core/assets/pmh/images/PMH_womensprofile.jpg)
Cautious grounds for optimism: Cochrane reviewers have changed their conclusions about progestogen for threatened miscarriage, now that some signs of effectiveness have emerged. More and better research is needed, though, to be sure if it works -- and what the optimum treatment would be.

Read their summary [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0013622/).

Citation: Wahabi HA, Fayed AA, Esmaeil SA, Al Zeidan RA. Progestogen for treating threatened miscarriage. Cochrane Database of Systematic Reviews: Plain Language Summaries [Internet]. 2011 Dec 7 [cited 2011 Dec 20]; (12): CD005943. Available from <http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0013622/>

Full review available from [The Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD005943.pub4/abstract).


