Featured review
---------------

![Image of woman](/core/assets/pmh/images/PMH_postnatal.jpg)

Preventing postnatal depression
===============================

More than 1 in 10 women experience depression within the first 3 months after giving birth. Postpartum or postnatal depression can be devastating, making women feel miserable, guilty and sometimes suicidal.

There are many theories about why it happens, but no definite specific cause has been proven.

Researchers from Canada and England have updated the Cochrane review on social and psychological support efforts to prevent postpartum depression. They drew conclusions by analyzing 28 trials involving almost 17,000 women.

The result? There are ways to reduce the risk of becoming deeply depressed after childbirth. Professional and lay support—in person or by telephone—have helped some women.

To find out more, you can read:

-   The researchers’ [summary of their review](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010878/)
-   An evidence-based [fact sheet on](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0005000/) depression after childbirth

<span>*Citation:* Dennis CL, Dowswell T. Psychosocial and psychological interventions for preventing postpartum depression. Cochrane Database of Systematic Reviews 2013, Issue 2. Art. No.: CD001134. DOI: 10.1002/14651858.CD001134.pub3. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD001134.pub3/abstract)</span>

*By PubMed Health, 5 March 2013.*

[More featured reviews](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=pmhfeaturedreviews)

[How do we know what works?](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)


