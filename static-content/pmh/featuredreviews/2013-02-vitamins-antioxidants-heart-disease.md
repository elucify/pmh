### Featured review

![Image of people in street](/core/assets/pmh/images/PMH_crowd.jpg)

Vitamin and antioxidant supplements to prevent heart disease: the case against
==============================================================================

Eat lots of fruit and vegetables: it’s an important health message to lower risk of cardiovascular (heart) disease. But taking vitamin and antioxidants supplements to try to achieve that goal? That’s a very different question.

A systematic review published by the British Medical Journal analyzed 50 randomized trials that studied the effects of these supplements on heart disease. Close to 300,000 people participated in these trials.

The researchers came to the conclusion that none of the supplements reduced the risk of developing heart disease or dying from it.

To find out more, you can read:

-   The DARE analysis and summary of [this systematic review on vitamin and antioxidant supplements](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0051981/)
-   The full systematic review via [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/?term=23335472)

<span>*Citation:* Myung SK, Ju W, Cho B, Oh SW, Park SM, Koo BK, Park BJ, Korean Meta-Analysis (KORMA) Study Group. Efficacy of vitamin and antioxidant supplements in prevention of cardiovascular disease: systematic review and meta-analysis of randomised controlled trials. BMJ 2013; 346: f10. [PMID 23335472](http://www.ncbi.nlm.nih.gov/pubmed/?term=23335472)</span>

*By PubMed Health, 6 February 2013.*

[More featured reviews](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=pmhfeaturedreviews)

[How do we know what works?](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)


