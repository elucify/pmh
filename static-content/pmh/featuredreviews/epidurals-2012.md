Featured review
---------------

![Image of mother and baby for featured review](/core/assets/pmh/images/PMH_Birth.png)
Mounting evidence on the effects of epidurals for labor and childbirth: this updated Cochrane review now has enough data to answer many key questions, including what happens to the risk of cesarean section.

Read the reviewers' summary [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010612/).

And read more about epidurals for labor and childbirth in this [fact sheet](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004976/%20).

Citation: Anim‐Somuah M, Smyth RMD, Jones L. Epidural versus non‐epidural or no analgesia in labour. Cochrane Database of Systematic Reviews: Plain Language Summaries [Internet]. 2011 Dec 7 [cited 2012 Jan 4]; (12): CD000331. Available from <http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010612/>.

Full review available from [The Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD000331.pub3/abstract).


