Featured review
---------------

![Image for featured review.](/core/assets/pmh/images/PMH_exercise_bike.png)
After a stroke, regaining any lost ability is understandably the priority. However, regaining physical fitness is a critical part of recovery, too. A Cochrane review assesses the evidence, and the Institute for Quality and Efficiency in Health Care (IQWiG) has just updated their consumer summary of it.

You can read the IQWiG consumer information [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033706/).

Citation: Brazzelli M, Saunders DH, Greig CA, Mead GE. Physical fitness training for stroke patients [review]. Cochrane Database of Systematic Reviews: Plain Language Summaries [Internet]. 2011 Nov 9 [cited 2012 Feb 7];(11);CD003316. Available from <http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0014857/>


