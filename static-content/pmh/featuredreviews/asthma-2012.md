Featured review
---------------

![Image of people riding bikes for featured review.](/core/assets/pmh/images/PMH_4bikes.jpeg)
Some people with asthma avoid exertion that might trigger attacks. But too little exercise can reduce lung fitness, which might worsen asthma.

So do the benefits outweigh the risks? According to this Cochrane review, the answer is yes.

The researchers from universities in Australia and England found 19 trials of physical training in people with asthma who were 8 years or older. You can read about their results [here](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0010867/%20).

More about asthma and exercise:

-   [Research](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033700/%20) on swimming and asthma
-   [Fact sheet](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0033704/) covering research on exercise-induced asthma

*Citation:* Chandratilleke MG, Carson KV, Picot J, Brinn MP, Esterman AJ, Smith BJ. Physical training for asthma. Cochrane Database of Systematic Reviews 2012, Issue 5. Art. No.: CD001116. DOI: 10.1002/14651858.CD001116.pub3. [Link to Cochrane Library](http://onlinelibrary.wiley.com/doi/10.1002/14651858.CD001116.pub3/full)


