![Image of woman swimming](/core/assets/pmh/images/PMH_womanswimming.jpg)

Featured review: Lubricant shots for knee osteoarthritis
========================================================

One of the results of osteoarthritis of the knees is that there is less fluid lubricating the joint. That can make it harder to move the joint, and increases osteoarthritis pain.

Viscosupplementation is the technical term for lubricant shots into the joint. What's injected is a gel-like material called a hyaluronic acid. There are multiple shots, and these can cause side effects like pain and swelling that sometimes does not go away.

Researchers from Bern University in Switzerland searched for published and unpublished trials on the benefits and harms of this commonly used treatment. They analyzed 89 trials with more than 12,000 people. Most of the trials included comparisons to sham treatment.

They concluded there is no clinically important benefit, but there is a risk of harm. To find out more, you can read:

-   The DARE analysis and summary of [this systematic review on viscosupplementation](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0048156/)
-   The researchers' [summary of their review](http://www.ncbi.nlm.nih.gov/pubmed/22868835)

You can also read more about viscosupplementation and other options for osteoarthritis of the knee by AHRQ, the Agency for Healthcare Quality and Research:

-   [Osteoarthritis of the knee: a guide for adults](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0004905/%20)
-   Or their guide for clinicians: [Three treatments for osteoarthritis of the knee](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0008368/)

<span>*Citation:* Rutjes AW, Juni P, da Costa BR, Trelle S, Nuesch E, Reichenbach S.  Viscosupplementation for osteoarthritis of the knee. Annals of Internal Medicine 2012; 157(30): 180-191. [PMID 22868835](http://www.ncbi.nlm.nih.gov/pubmed/22868835)</span>

*By PubMed Health, 28 January 2013.*

[More featured reviews](http://www.ncbi.nlm.nih.gov/feed/rss.cgi?ChanKey=pmhfeaturedreviews)

[How do we know what works?](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/)

[Understanding research results](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/)


