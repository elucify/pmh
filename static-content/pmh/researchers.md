![Systematic review methods](/core/assets/pmh/images/Methods-filter-2015.jpg)

For Researchers
===============

We specialize in [clinical effectiveness research](http://www.ncbi.nlm.nih.gov/pubmedhealth/aboutcer/), and information based on it, at PubMed Health. Clinical effectiveness research looks for answers to the question, “What works?”, in health and medicine.

We also develop resources to help researchers with the complex task of [systematic reviewing](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMHT0025759/). <span>Systematic reviewing is a key part of clinical effectiveness research – but it’s used across many other research areas, too.</span>

We gather resources to help everyone else understand the research process as well, including in [our glossary](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2014/04/information-support-tools/). Check out our [collection of free online books](http://www.ncbi.nlm.nih.gov/pubmedhealth/understanding-research-results/).

### Systematic review methods filter at PubMed

When you search in PubMed Health, you are simultaneously running two searches in PubMed. Results of those searches are shown to the right of the PubMed Health results.

The first of those PubMed searches looks for systematic reviews, using [the Clinical Queries filter](http://www.ncbi.nlm.nih.gov/pubmed/clinical). <span>Those results are under the “Systematic Reviews in PubMed” heading.</span>

<span>The second PubMed search uses a systematic review methods filter to find methods guidance and studies. Those results under the “Systematic Review Methods in PubMed” heading.</span>

Find out more about [what is included in the "Sysrev Methods" filter and how to use it](http://www.ncbi.nlm.nih.gov/pubmedhealth/researchers/pubmed-systematic-review-methods-filter/).

### Resources on effectiveness research methods at PubMed Health

PubMed Health is also a home for our information partners’ methods research guidance and studies. You will see these in the main results section of searches in PubMed Health. Read more about finding your way around the PubMed Health methods resources [on our blog](http://www.ncbi.nlm.nih.gov/pubmedhealth/blog/2015/06/research-methods/).

As these resources are added to PubMed Health, they are added to PubMed. So many are included in the PubMed results too. But the scope is a little wider at PubMed Health.

PubMed Health partners sometimes do methods research that is not specifically about systematic reviewing. For example, there are Cochrane methods reviews related to clinical trials (like [informed consent methods for trials](http://www.ncbi.nlm.nih.gov/pubmedhealth/PMH0012260/)). <span>These are all included at PubMed Health, but not in the PubMed filter.</span>

The agencies starting to contribute their methods resources so far are:

-   Agency for Healthcare Research and Quality (US) ([AHRQ](http://www.ahrq.gov/))
-   [Cochrane](http://www.cochrane.org/)
-   German Institute for Quality and Efficiency in Health Care ([IQWiG](https://www.iqwig.de/en/home.2724.html))
-   National Institute for Health and Care Excellence ([NICE](http://www.nice.org.uk/))
-   National Institute for Health Research (NIHR) Health Technology Assessment Programme ([NIHR HTA](http://www.nets.nihr.ac.uk/programmes/hta%20))
-   Institute of Medicine ([IOM](http://iom.nationalacademies.org/))

*The PubMed Health Team*
 *8 December 2015*


